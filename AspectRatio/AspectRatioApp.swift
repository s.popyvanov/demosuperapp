import SwiftUI

@main
struct AspectRatioApp: App {
    var body: some Scene {
        WindowGroup {
            AspectRatioAppContentView()
        }
    }
}
