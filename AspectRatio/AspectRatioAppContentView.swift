import SwiftUI

struct AspectRatioAppContentView: View {
    var body: some View {
        VStack {
            Text("AspectRatio")
        }
        .padding()
    }
}

struct AspectRatioAppContentView_Previews: PreviewProvider {
    static var previews: some View {
        AspectRatioAppContentView()
    }
}
