import SwiftUI

struct AspectRatioContentView: View {
    var body: some View {
        VStack {
            Text("AspectRatioApp")
        }
        .padding()
    }
}

struct AspectRatioContentView_Previews: PreviewProvider {
    static var previews: some View {
        AspectRatioContentView()
    }
}
