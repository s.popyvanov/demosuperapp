import SwiftUI
import HealthKit


@main
struct WhatchOSDemoApp_Watch_AppApp: App {
    
#if !TESTING
    var body: some Scene {
        WindowGroup {
            WatchOSDemoAppContentView()
        }
    }
#endif
    
#if TESTING
    var body: some Scene {
        WindowGroup {
            Text("Test")
        }
    }
#endif
    
    
}

