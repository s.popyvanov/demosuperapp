import WatchKit

enum BreathType: String {
    case inhale = "Вдох"
    case exhale = "Выдох"
    case holding = "Задержка"
    case change = "Смена упражнения"
    
    var hapticType: WKHapticType {
        return .click
    }
}
