import Foundation

struct RepeatingExercise: Equatable {
    let exercise: BreathExercise
    let count: Int
}

extension RepeatingExercise {
    static let changing = RepeatingExercise(exercise: .changeBreath, count: 1)
}

struct BreathExercise: Equatable {
    let name: String
    let sequence: [BreathType]
    let bpm: Int
    
    var exercisePeriod: Decimal {
        oneSequencePeriod * Decimal(sequence.count)
    }
    var oneSequencePeriod: Decimal {
        60 / Decimal(bpm)
    }
}

extension BreathExercise {
    static let falcon = BreathExercise(
        name: "Сокол",
        sequence: [.inhale, .exhale, .exhale, .exhale, .exhale, .exhale, .exhale, .exhale, .holding],
        bpm: 10
    )
    static let bear = BreathExercise(
        name: "Медведь",
        sequence: [.inhale, .inhale, .inhale, .inhale, .inhale, .inhale, .inhale, .exhale, .holding],
        bpm: 10
    )
    static let turtle = BreathExercise(
        name: "Черепаха",
        sequence: Array(repeating: BreathType.inhale, count: 5) + Array(repeating: BreathType.exhale, count: 5),
        bpm: 7
    )
    static let wolf = BreathExercise(name: "Волк", sequence: [.inhale, .exhale, .inhale, .exhale, .holding], bpm: 60)
    
    static let changeBreath = BreathExercise(name: "Смена упражнения", sequence: [.inhale, .inhale, .exhale, .exhale], bpm: 30)
}
