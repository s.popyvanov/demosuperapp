import SwiftUI
import UIKit

final class BreathExerciseManager: ObservableObject {
    private(set) var workoutManager = WorkoutManager()
    private let excersizes: [BreathExercise] = [.falcon, .bear, .turtle, .wolf]
        
    @Published var totalMinutes: Int = 20
    @Published var currentSequenceSeconds: Decimal = 0.0
    @Published var excersiseName: String = ""
    @Published var breatheType: String = ""
    
    @Published var isPlaying = false {
        didSet {
            isPlaying ? start() : stop()
            if !isPlaying { currentSequenceSeconds = 0 }
        }
    }
    
    // MARK: - Private Properties
    private var timer: Timer?
    private var sequenceCount: Int = 0
    private var repeatSequenceCount: Int = 1
    private(set) var insideExerciseCount: Int = 0
    private(set) var sequence: [RepeatingExercise] = []
    
    func start() {
        buildExecrcise()
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
        WKInterfaceDevice.current().play(BreathType.inhale.hapticType)

        workoutManager.startWorkout()
    }
    
    func stop() {
        timer?.invalidate()
        timer = nil
        sequenceCount = 0
        repeatSequenceCount = 0
        insideExerciseCount = 0
        currentSequenceSeconds = 0
        
        workoutManager.endWorkout()
    }
    
    @objc func onTick() {
        guard sequence.indices.contains(sequenceCount) else {
            print("Нужно настроить упражнения")
            return
        }
        let repeatEx = sequence[sequenceCount]
        let currentEx = repeatEx.exercise
        
        guard currentSequenceSeconds >= currentEx.oneSequencePeriod * (Decimal(insideExerciseCount + 1))  else {
            currentSequenceSeconds += 0.1
            return
        }
        insideExerciseCount += 1
        
        let isOutOfRangeInsideExerciseCount = !currentEx.sequence.indices.contains(insideExerciseCount)
        guard isOutOfRangeInsideExerciseCount else {
            let nextBreathType = currentEx.sequence[insideExerciseCount]
            changeTypeAndPlayFeedBack(nextBreathType)
            return
        }
        
        currentSequenceSeconds = 0
        insideExerciseCount = 0
        let isNeedToRepeatExercise = repeatSequenceCount <= repeatEx.count
        
        guard !isNeedToRepeatExercise else {
            repeatSequenceCount += 1
            let currentExType = currentEx.sequence[insideExerciseCount]
            changeTypeAndPlayFeedBack(currentExType)
            return
        }
        sequenceCount += 1
        repeatSequenceCount = 1
        
        let isOutOfRangeRepeatExercise = !sequence.indices.contains(sequenceCount)
        guard isOutOfRangeRepeatExercise else {
            let currentSequence = sequence[sequenceCount]
            excersiseName = currentSequence.exercise.name
            let currentExType = currentSequence.exercise.sequence[insideExerciseCount]
            changeTypeAndPlayFeedBack(currentExType)
            return
        }
        
        excersiseName = "Все"
        breatheType = "Конец упражнений"
        isPlaying.toggle()
    }
    
    private func changeTypeAndPlayFeedBack(_ type: BreathType) {
        WKInterfaceDevice.current().play(type.hapticType)
        self.breatheType = type.rawValue
    }
    
    func buildExecrcise() {
        excersiseName = excersizes.first?.name ?? ""
        breatheType = BreathType.inhale.rawValue
        var result: [RepeatingExercise] = []
        let oneExcersizePeriod = Decimal(totalMinutes/excersizes.count) * 60
        excersizes.forEach { exercise in
            let exerciseTimes = ((oneExcersizePeriod / exercise.exercisePeriod) as NSDecimalNumber).intValue
            result.append(
                RepeatingExercise(exercise: exercise, count: exerciseTimes)
            )
            
            result.append(.changing)
        }
        self.sequence = result
    }
}
