import SwiftUI

struct TimeContentView: View {
    @StateObject var excerciseManager = BreathExerciseManager()

    var body: some View {
        VStack {
            Spacer()
            Image(systemName: excerciseManager.isPlaying ? "pause.circle" : "play")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Упражнение \(excerciseManager.excersiseName)")
            Text(excerciseManager.breatheType)
            HStack {
                Button(
                    action: { excerciseManager.totalMinutes -= 1},
                    label: { Image(systemName: "minus.square") }
                )
                Text("\(excerciseManager.totalMinutes)")
                Button(
                    action: { excerciseManager.totalMinutes += 1},
                    label: { Image(systemName: "plus.app") }
                )
            }
            Spacer()
        }
        .onTapGesture {
            excerciseManager.isPlaying.toggle()
        }
        .padding()
        .onAppear {
            excerciseManager.workoutManager.requestAuthorization()
        }
    }
}
