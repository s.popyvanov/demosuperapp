import SwiftUI

struct WatchOSDemoAppContentView: View {
    
    @State private var currentPage = 0

    var body: some View {
        PagerManager(pageCount: 2, currentIndex: $currentPage) {
            TimeContentView()
            Text("SecondScreen")
        }
    }
    

}
