import XCTest
@testable import WhatchOSDemoApp_Watch_App

final class WhatchOSDemoApp_Watch_AppTests: XCTestCase {
    
    var manager: BreathExerciseManager!

    override func setUp() {
        manager = BreathExerciseManager()
    }

    override func tearDown() {
        manager = nil
    }

}
