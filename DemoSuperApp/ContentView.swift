import SwiftUI

struct ContentView: View {
    
    private let router: Router
    
    init(router: Router) {
        self.router = router
    }
    
    var body: some View {
        HStack {
            Image(systemName: "aspectratio")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("AspectRatio App")
        }.onTapGesture {
            router.showAspectRationApp()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(router: Router())
    }
}
