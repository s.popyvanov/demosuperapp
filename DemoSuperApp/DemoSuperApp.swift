import SwiftUI

@main
struct DemoSuperApp: App {
    @ObservedObject var router = Router()
    
    var body: some Scene {
        WindowGroup {
            NavigationStack(path: $router.path) {
                ContentView(router: router)
                    .navigationDestination(for: Route.self) { route in
                        switch route {
                        case .aspectRatioApp:
                            AspectRatioAppContentView()
                        }
                    }
            }
        }
    }
}
