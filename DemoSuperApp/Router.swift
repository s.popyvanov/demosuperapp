import SwiftUI

enum Route: String {
    case aspectRatioApp
}

final class Router: ObservableObject {
    @Published var path = [Route]()
    
    func showAspectRationApp() {
        path.append(.aspectRatioApp)
    }
    
    func backToRoot() {
        path.removeAll()
    }
    
    func back() {
        path.removeLast()
    }
}
